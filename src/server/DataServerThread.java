package server;

import java.util.*;
import java.io.*;
import java.net.*;
import json.*;
import javax.swing.*;
import java.awt.*;

public class DataServerThread extends Thread {
	private Socket connection;
	private Map<String,ServerRFIDObject> data;
	private File jsonFile;
	private int size;
	private JFrame window;
	private UI canvas;
	private int numPublish;
	
	
	public DataServerThread(Socket connection, int size) {
		super("DataServerThread");
		this.connection = connection;
		this.size = size;
		numPublish = 0;
		data = new HashMap<String,ServerRFIDObject>();
		window = new JFrame("RFID Data Server");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setBackground(Color.white);
		window.setSize(1000, 600);
		canvas = new UI();
		canvas.setSize(1000,600);
		window.add(canvas);
		window.pack();
		window.setVisible(true);
	}
	
	public void run() {
		try {
			System.out.println("Accepted connection from "+connection.getInetAddress().toString());
			BufferedReader input = new BufferedReader( new InputStreamReader(connection.getInputStream()));
			PrintWriter output = new PrintWriter(connection.getOutputStream(), true);
			output.println("Connection Initiated. Send file name: ");
			String inputLine = "", name = "", type = "";
			String[] inputs;
			Double x = 0.0, y = 0.0, z = 0.0, lastSeen = 0.0;
			String id = "";
			ServerRFIDObject current;
			inputLine = input.readLine();
			output.println("File name is: " + inputLine);
			try {
				this.jsonFile = new File(inputLine);
				jsonFile.createNewFile();
				while(!connection.isClosed()) {
					inputLine = input.readLine();
					switch(inputLine) {
					case "end":
						output.println("Read an end request.");
						input.close();
						output.close();
						connection.close();
						break;
					case "publish":
						output.println("Published data to "+jsonFile.getPath());
						this.publishJSON(data, jsonFile);
						break;
					default:
						//Note: If the client keeps a record of observed objects, we could change this so that ID and type must only be 
						//sent on the first transmission of data for a given object.
						inputs = inputLine.split(";");
						output.println("Read Data Beginning With: "+inputs[0]);
						name = inputs[0];
						if(!data.containsKey(name)) {
							type = inputs[1];
							id = inputs[2];
							lastSeen = 0.0;
							current = new ServerRFIDObject(this.size, id, type, lastSeen);
							data.put(name, current);
						}
						current = data.get(name);
						lastSeen = Double.parseDouble(inputs[3]);
						x = Double.parseDouble(inputs[4]);
						y = Double.parseDouble(inputs[5]);
						z = Double.parseDouble(inputs[6]);
						current.addPoint(x,y,z);
						current.updateSeen(lastSeen);
						break;
					}
				}
			}
			catch(ArrayIndexOutOfBoundsException n) {
				System.out.println("Error: Array ran out of elements");
				n.printStackTrace();
			}
		}
		catch(IOException e) {
			System.out.println("Error: Problem in reading from socket.");
			e.printStackTrace();
		}
		catch(NoSuchElementException n) {
			System.out.println("Error: File name wasn't sent");
			n.printStackTrace();
		}
	}
	private void publishJSON(Map<String, ServerRFIDObject> data, File jsonFile) {
		ArrayList<Point.Double> coordinates = new ArrayList<Point.Double>();
		try {
			JSONArray jA = new JSONArray();
			//JSONArray jO;
			ServerRFIDObject current = null;
			Double[] position;
			String json = "{\"objectList\":[";
			for(String s: data.keySet()) {
				//jO = new JSONArray();
				current = data.get(s);
				position = current.getMovingAveragePosition();
				coordinates.add(new Point.Double(position[0],position[1]));
				json = json.concat("{\"name\":\""+s
						+"\",\"xcoord\":"+position[0]
						+",\"ycoord\":"+position[1]
						+",\"zcoord\":"+position[2]
						+",\"objectID\":\""+current.getID()
						+"\",\"lastSeen\":"+current.getSeen()
						+",\"objectType\":\""+current.getType()
						+"\"},");
				/*jA.put(new JSONObject().put("name", s));
				jA.put(new JSONObject().put("xcoord", position[0]));
				jA.put(new JSONObject().put("ycoord", position[1]));
				jA.put(new JSONObject().put("zcoord", position[2]));
				jA.put(new JSONObject().put("objectID", current.getID()));
				jA.put(new JSONObject().put("lastSeen", current.getSeen()));
				jA.put(new JSONObject().put("objectType", current.getType()));
				*/
				//jA.put(jO);
			}
			json = json.substring(0, json.length()-1).concat("]}");
			
			synchronized(jsonFile) {
				/*objectJSON.write("{ \"objectList\": ");
				objectJSON.write(jA.toString());
				objectJSON.write("}");*/
				FileWriter objectJSON = new FileWriter(jsonFile, false);
				jsonFile.delete();
				objectJSON.write(json);
				objectJSON.close();
			}
			++numPublish;
			System.out.println("Published to "+jsonFile.getPath()+", publish number " + numPublish);
			canvas.paintComponent(canvas.getGraphics(), coordinates);
		}
		catch(IOException e) {
			System.out.println("Error: Problem writing to or deleting file.");
			e.printStackTrace();
		}
		/*catch(JSONException j) {
			System.out.println("Error: Problem with JSON");
			j.printStackTrace();
		}*/
	}

}
