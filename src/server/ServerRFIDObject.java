package server;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class ServerRFIDObject {
	private ArrayBlockingQueue<Double> xMeasurements;
	private ArrayBlockingQueue<Double> yMeasurements;
	private ArrayBlockingQueue<Double> zMeasurements;
	private String type;
	private String id;
	private double lastSeen;
	
	public ServerRFIDObject(int size, String id, String type, double lastSeen) {
		xMeasurements = new ArrayBlockingQueue<Double>(size,true);
		yMeasurements = new ArrayBlockingQueue<Double>(size,true);
		zMeasurements = new ArrayBlockingQueue<Double>(size,true);
		this.id = id;
		this.type = type;
		this.lastSeen = lastSeen;
	}
	
	public void updateSeen(double lastSeen) {
		this.lastSeen = lastSeen;
	}
	
	public double getSeen() {
		return this.lastSeen;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getID() {
		return this.id;
	}
	
	public void addPoint(Double x, Double y, Double z) {
		//if(x < 0 || y < 0 || z < 0) return;
		//else {
			this.addX(x);
			this.addY(y);
			this.addZ(z);
		//}
	}
	private void addX(Double x) {
		if(xMeasurements.remainingCapacity() == 0) {
			xMeasurements.remove();
		}
		if(!xMeasurements.offer(x)) {
			System.out.println("RFIDObject Error: Couldn't add element to queue, even after clearing space.");
		}
	}
	
	private void addY(Double y) {
		if(yMeasurements.remainingCapacity() == 0) {
			yMeasurements.remove();
		}
		if(!yMeasurements.offer(y)) {
			System.out.println("RFIDObject Error: Couldn't add element to queue, even after clearing space.");
		}
	}
	
	private void addZ(Double z) {
		if(zMeasurements.remainingCapacity() == 0) {
			zMeasurements.remove();
		}
		if(!zMeasurements.offer(z)) {
			System.out.println("RFIDObject Error: Couldn't add element to queue, even after clearing space.");
		}
	}
	
	public Double[] getMovingAveragePosition() {
		Double[] position = new Double[3];
		Double x = 0.0, y = 0.0, z = 0.0;
		for(Double d: xMeasurements) 
			x += d;
		for(Double d: yMeasurements)
			y += d;
		for(Double d: zMeasurements) 
			z += d;
		position[0] = x/(double)xMeasurements.size();
		position[1] = y/(double)yMeasurements.size();
		position[2] = z/(double)zMeasurements.size();
		return position;
	}
	
	public Double[] getWeightedAveragePosition() {
		Double[] position = new Double[3];
		Double x = 0.0, y = 0.0, z = 0.0;
		int n = 0;
		double xFactor = 0.5*(Math.pow(xMeasurements.size(),2) + xMeasurements.size());
		double yFactor = 0.5*(Math.pow(yMeasurements.size(),2) + yMeasurements.size());
		double zFactor = 0.5*(Math.pow(zMeasurements.size(),2) + zMeasurements.size());
		for(Double d: xMeasurements) 
			x += d*(++n/xFactor);
		n = 0;
		for(Double d: yMeasurements) 
			y += d*(++n/yFactor);
		n = 0;
		for(Double d: zMeasurements) 
			z += d*(++n/zFactor);
		position[0] = x;
		position[1] = y;
		position[2] = z;
		return position;
	}
	
	public Double[] getMovingAveragePositionWithoutOutliers() {
		ArrayList<Double> xValues = new ArrayList<Double>(xMeasurements);
		ArrayList<Double> yValues = new ArrayList<Double>(yMeasurements);
		ArrayList<Double> zValues = new ArrayList<Double>(zMeasurements);
		Double[] position = this.getMovingAveragePosition();
		Double xStdDev = this.calculateStdDev(position[0],xValues);
		Double yStdDev = this.calculateStdDev(position[1],yValues);
		Double zStdDev = this.calculateStdDev(position[2],zValues);
		Double x = 0.0, y = 0.0, z = 0.0;
		for(int n = 0; n < xValues.size(); ++n) {
			if((Math.abs(xValues.get(n) - position[0]) > 0.8*xStdDev) || 
					(Math.abs(yValues.get(n) - position[1]) > 0.8*yStdDev) || 
					(Math.abs(zValues.get(n) - position[2]) > 0.8*zStdDev)) {
				xValues.remove(n);
				yValues.remove(n);
				zValues.remove(n);
			}
		}
		if(xValues.isEmpty() || yValues.isEmpty() || zValues.isEmpty()) 
			System.out.println("Error: All values more than 1 standard deviations from mean.");
		for(Double d: xValues)
			x += d;
		for(Double d: yValues)
			y += d;
		for(Double d: zValues)
			z += d;
		position = new Double[3];
		position[0] = x/(double)xValues.size();
		position[1] = y/(double)yValues.size();
		position[2] = z/(double)zValues.size();
		return position;
	}
	
	private Double calculateStdDev(Double u, ArrayList<Double> vals) {
		Double stdDev = 0.0;
		Double sum = 0.0;
		for(Double d: vals) {
			sum += Math.pow(d-u, 2);
		}
		sum *= (1/vals.size());
		stdDev = Math.sqrt(sum);
		return stdDev;
	}
	//TODO Add change size method
}
