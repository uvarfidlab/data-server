package server;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class UI extends JPanel {
	
	public Dimension getPreferredSize() {
		return new Dimension(1000,600);
	}
	
	public void paintComponent(Graphics g, ArrayList<Point.Double> a) {
		super.paintComponent(g);
		g.setColor(Color.red);
		for(Point.Double p: a) {
			int x = (int)((p.getX()/2.29235)*(1000)-10);
			int y = (int)((p.getY()/1.222375)*(600)-10);
			//System.out.println("X,Y: "+x+","+y);
			g.fillOval(x, y, 10, 10);
		}
	}
}
