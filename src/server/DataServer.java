package server;

import java.net.*;
import java.io.*;
import java.util.*;

public class DataServer {
	public static void main(String args[]) throws IOException {
		ServerSocket listener = null;
		String end = "";
		Scanner keyboard = new Scanner(System.in);
		File jsonFile;
		try {
			Scanner settings = new Scanner( new File("serversettings.cfg"));
			int rfidSize = settings.nextInt();
			jsonFile = new File(settings.next());
			settings.close();
			listener = new ServerSocket(1819);
			while(true) {
				System.out.println("Listening....");
				new DataServerThread(listener.accept(), rfidSize).start();
			}
			//listener.close();
		}
		catch(IOException e) {
			System.out.println("Error Creating Socket:");
			e.printStackTrace();
			System.exit(-1819);
		}
		
	}
}
